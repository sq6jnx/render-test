FROM python
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY src/main.py .

EXPOSE 8000
ENTRYPOINT gunicorn 'main:app' --bind 0.0.0.0
